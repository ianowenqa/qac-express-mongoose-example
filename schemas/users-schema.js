var mongoose = require('mongoose');

/** 
 * Schema for a generic user.
 */
exports.UsersSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true,
        maxlength: 255
    },
    lastName: {
        type: String,
        required: true,
        maxlength: 255
    },
    age: {
        type: Number,
        default: 0,
        min: 0,
        max: 140,
        required: true
    }
});

/**
 * User model definition. This will use a connection called 'users'.
 */
exports.UserModel = mongoose.model('User', this.UsersSchema);