var mongoose = require('mongoose');
var express = require('express');
var config = require('./config');
var utils = require('./utils');
var UserRoutes = require('./routes/user-routes');
var cors = require('cors');
var app = express();

// Include CORS middleware
app.use(cors({origin: true}));
// Automatically parse request bodies as JSON
app.use(express.json());

// Routes middleware
app.use('/user',
    // (req, _res, next) => {
    //     try {
    //         req.userId = utils.toObjectId(req.params.userId);
    //         next();
    //     } catch (exc) {
    //         next({ message: exc.message });
    //     }
    // },
    UserRoutes
); // Hook the user routes up to '/user'


// Example endpoint
app.get('/', (req, res) => {
    res.send('Hello, World!');
});

// router.get('/users/all', (req, res) => {
//     console.log(req.baseParams);
//     return Users.UserModel.find().then((doc) => {
//         return res.send(doc);
//     });
// });


// Custom error handler middleware. The default HTML errors are cumbersome.
app.use((err, req, res, next) => {
    if (config.app.logErrors) {
        console.error(err);
    }
    return res.status(500).send(err);
});


// Start up application. First, connect to the MongoDB instance.
mongoose.connect(
    config.app.MONGODB_URI,
    { useNewUrlParser: true })
    .then((res) => {
        console.log('Connection to MongoDB established.');
    }, (error) => {
        console.error('Failed to connect to MongoDB. Exitting.');
        console.error(error);
        process.exit(1);
    }).then(() => {
        // If the connection is successful, set up Express to listen for incoming requests.
        app.listen(config.app.PORT, () => {
            console.log(`Server running on port ${config.app.PORT}`);
        });
    });
